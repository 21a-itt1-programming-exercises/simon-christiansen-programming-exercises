'''
Exercise 1: Write a simple program to simulate the operation of the
grep command on Unix. Ask the user to enter a regular expression and
count the number of lines that matched the regular expression:

$ python grep.py
Enter a regular expression: ^Author
mbox.txt had 1798 lines that matched ^Author

$ python grep.py
Enter a regular expression: ^X-
mbox.txt had 14368 lines that matched ^X-

$ python grep.py
Enter a regular expression: java$
mbox.txt had 4175 lines that matched java$
'''

#!/usr/bin/python3
import re # importing regular expression module

count = 0 # Initiating a variable to act as a counter

reg_ex_input = str(input("Please enter a regular expression: ")) # Asks for user input and converts it to a string
filename = 'mbox.txt'
filehandle = open(filename)

for line in filehandle: # Opens the file and strips every line
	line = line.rstrip()

	if re.findall(reg_ex_input, line) != []: # If the the line contains the input regex, add one to count
		count += 1

print(f'There is {count} lines in {filename} that matched {reg_ex_input}')