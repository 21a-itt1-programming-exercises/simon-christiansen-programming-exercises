# Week 37 Exercises

Reference: Py4e exercises - ch2_ex2-5.py

## Exercise 0

### Difference between syntax, logical and semantic errors

**Syntax errors:**
Basically it is when some grammar is wrong or you forgot a colon somewhere. 

**Logical errors:**
Good syntax, but the statements is not in a logical order. 

**Semantic errors:**
With semantic errors, you described something wrong and there is an error when the program is trying to follow your commands. Semantic errors will not give you an error, it just produces the wrong output.

**What is input and output?**
The input is what i put into the code and the output is whatever it prints out.

**What is sequential execution?**
Means that the statements is executed in the order they are listed in the code.

**Which four things should we do when debugging?**
Reading
Running
Ruminating
Retreat

## Exercise 1

What types of values is Python using?
integers, floats, strings, boolean

How would you check a value type in a Python program?
```python
# like this
type('whatefter')
# or like this
type(62.32)
```

**What are variables?**
Variables is something we tell the program to remember so we can use it alter in the code.

**What are reserved words?**
Reserved words is keywords Python uses for specific things. There is 35 listed words in the book.

**What is a statement?**
A statement is what we tell the code to execute, e.g.

```python
# this is the 'assign' statement
a = 'mother'
b = 'fucker'
# this is the 'print' statement
print("Hey " + a + b + "!)
```

## Exercise 2

**What is the purpose of mnemonic naming?**
Naming convention based on common sentences (usually the first letter of the word)

**Give at least 3 examples of mnemonic naming**
All People Seem To Need Data Processing

**Name pythons 6 operators and their syntax**
+, -, *, /, **, //

**What is order of operations?**
(), *, /, +, -

**What is concatenation?**
Joining strings

**How do you ask the user for input in a Python program?**
input()

**How do you insert comments in a Python program?**
'# comment